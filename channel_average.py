import sys
import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]
bandstart = int(sys.argv[2])
bandend = int(sys.argv[3])

fildata = (np.reshape(np.fromfile(filename, dtype='B')[277:], (-1, 4096))).T
bandpass = np.sum(fildata, axis=1)

avg = np.mean(bandpass[bandstart: bandend])
print("Average beteeen channels %d and %d is %f" % (bandstart, bandend, avg))
