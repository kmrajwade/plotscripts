import sys
import numpy as np
import matplotlib.pyplot as plt

bandstart = int(sys.argv[1])
bandend = int(sys.argv[2])

for ifile in np.arange(len(sys.argv) - 3) + 3:
	filename = sys.argv[ifile]
	fildata = (np.reshape(np.fromfile(filename, dtype='B')[277:], (-1, 4096))).T
	print(fildata[4000, 950:1000])
	bandpass = np.sum(fildata, axis=1)
	avg = np.mean(bandpass[bandstart: bandend])
	print("Average for file %s beteeen channels %d and %d is %f" % (filename, bandstart, bandend, avg))
