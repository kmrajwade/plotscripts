import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

bandstart = int(sys.argv[1])
bandend = int(sys.argv[2])

numfiles = len(sys.argv) - 3
print(numfiles)

fig = plt.figure(figsize=(15,15 * numfiles))
#fig, axis = plt.subplots(len(sys.argv - 3), 1, figsize=(leng(sys.argv -3) * 15, 10))

main_grid = gs.GridSpec(numfiles, 1)

for ifile in np.arange(len(sys.argv) - 3) + 3:
        
        file_grid = gs.GridSpecFromSubplotSpec(1, 2, subplot_spec=main_grid[ifile - 3])
        band_grid = gs.GridSpecFromSubplotSpec(3, 1, subplot_spec=file_grid[0], hspace=0.25)
        fil_grid = gs.GridSpecFromSubplotSpec(3, 1, subplot_spec=file_grid[1], hspace=0.25)

        axband = plt.Subplot(fig, band_grid[:-1, :])
        axsubband = plt.Subplot(fig, band_grid[-1:, :])

        axfil = plt.Subplot(fig, fil_grid[:-1, :])
        axhist = plt.Subplot(fig, fil_grid[-1:, :])
        

        filename = sys.argv[ifile]
        print(filename)
	fildata = (np.reshape(np.fromfile(filename, dtype='B')[277:], (-1, 4096))).T
	timesamples = np.minimum(fildata.shape[1], 1024)

        bandpass = np.sum(fildata, axis=1)
        avg = np.mean(bandpass[bandstart: bandend])

        axband.plot(bandpass, color='deepskyblue')
        axband.axvspan(bandstart, bandend, alpha=0.5, color='firebrick')
        axband.set_yscale('log')
        axband.set_xlabel('Channel number')
        axband.set_ylabel('Power')
        axband.set_title(filename)
        axsubband.plot(bandpass[bandstart:bandend], color='firebrick')
        axsubband.axhline(avg, linestyle='--', color='orange', label='Avg: %.2f' % avg)
        axsubband.set_yscale('log')
        axsubband.legend()	
        axsubband.set_xticks(np.arange(0,bandend - bandstart, int(np.ceil(bandend - bandstart) / 6)))
        axsubband.set_xticklabels(np.arange(bandstart, bandend, int(np.ceil(bandend - bandstart) / 6))) 
        axsubband.set_xlabel('Channel number')
        axsubband.set_ylabel('Power')
        fig.add_subplot(axband)
        fig.add_subplot(axsubband)
	print("Average for file %s beteeen channels %d and %d is %f" % (filename, bandstart, bandend, avg))

        axfil.imshow(fildata[bandstart:bandend, 0:timesamples], aspect='auto')
        axfil.set_xlabel('Time sample')
        axfil.set_ylabel('Channel number')
        axhist.hist(fildata[bandstart:bandend, 0:timesamples].flatten() - 1, bins=256, histtype='step', color='deepskyblue')
        axhist.set_xlabel('Value')
        axhist.set_ylabel('Counts')
        axhist.set_yscale('log')
        fig.add_subplot(axfil)
        fig.add_subplot(axhist)

	#ax.imshow(fildata[bandstart:bandend, 1024:2048], aspect='auto')
        #ax.set_title(filename)


plt.savefig('diagnostics.png')
