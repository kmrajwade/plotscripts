import sys
import numpy as np
import matplotlib.pyplot as plt

bandstart = int(sys.argv[1])
bandend = int(sys.argv[2])

for ifile in np.arange(len(sys.argv) - 3) + 3:
	filename = sys.argv[ifile]
	fildata = (np.reshape(np.fromfile(filename, dtype='B')[277:], (-1, 4096))).T
	channel_std = np.std(fildata, axis=1)
	print(channel_std[bandstart:bandend])
	print(channel_std[bandstart:bandend].std())
	print(channel_std[bandstart:bandend].mean())
