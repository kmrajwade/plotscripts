import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

nchans = int(sys.argv[1])

fig = plt.figure(figsize=(15,10))
ax = fig.gca()

for ifile in np.arange(len(sys.argv) - 2) + 2:
        fildata = (np.reshape(np.fromfile(sys.argv[ifile], dtype='B')[277:], (-1, nchans))).T
        bandpass = np.sum(fildata, axis=1)
        ax.plot(bandpass, label=sys.argv[ifile], linestyle='--')

plt.legend()
fig.savefig('compare_bandpass.png')
