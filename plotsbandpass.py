import sys
import numpy as np
import matplotlib.pyplot as plt

filename = sys.argv[1]

fildata = (np.reshape(np.fromfile(filename, dtype='B')[277:], (-1, 4096))).T
bandpass = np.sum(fildata, axis=1)

fig = plt.figure(figsize=(15,10))
ax = fig.gca()
ax.plot(bandpass)
fig.savefig(filename + '.png')
